# oid

TODO: Write a description here

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
dependencies:
  oid:
    github: your-github-user/oid
```
2. Run `shards install`

## Usage

```crystal
require "oid"
```

TODO: Write usage instructions here

## Development

To pull submodules: `git submodule update --init --recursive`

## Contributing

1. Fork it (<https://github.com/your-github-user/oid/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Credit

Vector logic/code:

https://github.com/ajselvig/crystal_vector_math
https://github.com/garnet-engine/math
https://github.com/unn4m3d/crystaledge

## Contributors

- [Holden Omans](https://github.com/your-github-user) - creator and maintainer
